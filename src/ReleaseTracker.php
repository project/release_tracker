<?php

namespace Drupal\release_tracker;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class ReleaseTracker
 *
 * Service class to handle all config changes for release tracker.
 *
 * @package Drupal\release_tracker
 */
class ReleaseTracker implements ReleaseTrackerInterface {

  /**
   * The editable config for release tracker.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructor for the ReleaseTracker class.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->getEditable('release_tracker.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function bump($type = NULL) {

    if (!in_array($type, ['major', 'minor', 'patch'])) {
      throw new \InvalidArgumentException("Type must be one of 'major', 'minor' or 'patch'.");
    }
    $newRelease = NULL;
    switch ($type) {
      case 'patch':
        $newRelease = $this->nextPatchReleaseNumber();
        break;
      case 'minor':
        $newRelease = $this->nextMinorReleaseNumber();
        break;
      case 'major':
        $newRelease = $this->nextMajorReleaseNumber();
        break;
    }

    $this->config->set('release', $newRelease);
    $this->config->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentRelease() {
    return $current_release_string = $this->config->get('release');
  }


  /**
   * {@inheritdoc}
   */
  public function setReleaseNumber($release_number) {
    if (!$this->validateReleaseNumber($release_number)) {
      throw new \InvalidArgumentException("Invalid release number given.");
    }
    $this->config->set('release', $release_number);
    $this->config->save();
  }

  /**
   * Validates the passed number as a valid release number.
   *
   * @param string $number
   *   The number to validate.
   *
   * @return bool
   *   Returns TRUE if the passed number is a valid release number, FALSE
   *   otherwise.
   */
  protected function validateReleaseNumber($number) {
    if (preg_match("/^[0-9]+\.[0-9]+\.[0-9]+$/", $number)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @inheritDoc
   */
  public function nextMajorReleaseNumber() {
    return $this->nextReleaseNumber(0);
  }

  /**
   * @inheritDoc
   */
  public function nextMinorReleaseNumber() {
    return $this->nextReleaseNumber(1);

  }

  /**
   * @inheritDoc
   */
  public function nextPatchReleaseNumber() {
    return $this->nextReleaseNumber(2);
  }

  protected function nextReleaseNumber($indexToBump) {
    $curRelease = explode('.', $this->getCurrentRelease());
    $curRelease[$indexToBump]++;
    for ($i = $indexToBump + 1; $i < count($curRelease); $i++) {
      $curRelease[$i] = 0;
    }
    return implode('.', $curRelease);
  }

}
